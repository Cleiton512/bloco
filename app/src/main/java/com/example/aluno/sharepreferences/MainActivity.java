package com.example.aluno.sharepreferences;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText edtText;
    Notas mNotas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtText = findViewById(R.id.edtText);
        mNotas = new Notas(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNotas.SalvarNotas(edtText.getText().toString());
            }
        });


    }
    @Override
    protected void onStart() {
        super.onStart();
        edtText.setText(mNotas.RecuperarNotas());
    }

    @Override
    protected void onStop() {
        super.onStop();
        mNotas.SalvarNotas(edtText.getText().toString());
    }
}
